package HomeWorks.lesson17.task2;

import java.util.LinkedList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        LinkedList<Object> linkedList = new LinkedList<>();
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 4; i++) {
            linkedList.add(i);
        }
        System.out.println("sizin linkedList: " + linkedList);

        System.out.println("elave etmek istediyiniz eded:");
        int a = scanner.nextInt();
        System.out.println("hansi indekse elave etmek istiyirsiniz?");
        int index = scanner.nextInt();
        linkedList.add(index, a);
        System.out.println("son netice linkedList: " + linkedList);
     }
}
